<?php
/*--------------------------------------

				Auth

--------------------------------------*/

require_once 'vendor/php/WrapXML.php';
require_once 'vendor/php/GN21Search.php';


function getGN21User($username, $password) {

	$searchCriteria = '
	<Username>'.$_POST['username'].'</Username>
	<Password>'.$_POST['password'].'</Password>
	';

	$xmlReturn = new WrapXML(GN21Search::gN21_API_call(GN21Search::construct_soap_xml($searchCriteria, 'MemberService', 'FetchMemberByCredentials'), 'MemberService', 'FetchMemberByCredentials'));
	$profileReturn = $xmlReturn->get_node_value('MemberList');

	// Check if member is returned
	if(!empty($profileReturn)) {

		$NetworkId = $xmlReturn->get_node_value('NetworkId');
		$SourceUserId = $xmlReturn->get_attribute_value('Member','SourceUserId');

		$ssoTokenSearchCriteria = '
		<NetworkId>'.$NetworkId.'</NetworkId>
		<SourceUserId>'.$SourceUserId.'</SourceUserId>
		';

		$ssoTokenXML = new WrapXML(GN21Search::gN21_API_call(GN21Search::construct_soap_xml($ssoTokenSearchCriteria, 'MemberService', 'FetchMemberSSOToken'), 'MemberService', 'FetchMemberSSOToken'));
		$memberGUID = $ssoTokenXML->get_node_value('MemberGUID');

		$return = array(
			'status' => 'success',
			'message' => 'User exists in GN21 database',
			'response' => $memberGUID
		);
	
	} else {

		$return = array(
			'status' => 'error',
			'message' => 'You do not have an account. Please create a guest account!',
			'response' => 'null'
		);
	}
	return $return;
}