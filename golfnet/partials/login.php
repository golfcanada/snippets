<?php include 'header.php'; ?>

<h3 class='center-text'>Welcome to the new <a href='http://www.golfcanada.ca' target='_blank'>Golf Canada</a> Members Area</h3>
<p class='center-text thin'>Please login with your Golf Canada member username and password below</p>

<?php if(isset($flash['message'])) : ?>
	<div class='alert'><?= $flash['message']; ?></div>
<?php endif; ?>

<form id='login' method='post' action='./login' data-parsley-validate>
	<label for='username'>Username</label>
	<span id='username'></span>
	<input type='text' name='username' placeholder='Username' data-parsley-errors-container='#username' required>
	<label for='password'>Password</label>
	<span id='password'></span>
	<input type='password' name='password' placeholder='******' data-parsley-errors-container='#password' required>
	<input type='submit' value='Login' class='button'>
</form>

<?php include 'footer.php'; ?>