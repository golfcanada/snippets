<html>
	<head>
		<meta charset='utf-8'>
		<title>Golf Canada - Members</title>
		<link href='vendor/css/base.css' rel='stylesheet' type='text/css'>
		<link href='css/styles.css' rel='stylesheet' type='text/css'>

		<script src='vendor/js/jquery.min.js' type='text/javascript'></script>
		<script src='vendor/js/jquery.resize.min.js' type='text/javascript'></script>
		<script src='vendor/js/jquery.cookie.min.js' type='text/javascript'></script>
		<script src='vendor/js/parsley.min.js' type='text/javascript'></script>
		<script src='js/scripts.js' type='text/javascript'></script>
	</head>
	<body>

		<noscript>
			Your browser does not support JavaScript!
		</noscript>

		<div id='header'>
			<a href='http://www.golfcanada.ca' target='_blank'>
				<img src='images/gc_logo.png'/>
			</a>

			<?php if(isset($_SESSION['memberGUID'])) : ?>
				<!-- <h4 id='firstname'></h4> -->
				<a href='#' id='logout' class='button'>Logout</a>
			<?php endif; ?>
		</div>

		<div class='container'>