<?php
require 'auth.php';
require 'vendor/php/Slim/Slim.php';

//----- Report Errors
error_reporting(E_ALL);
ini_set('display_errors', '1');

//----- Start Sessions
session_start();


//----- App Config
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim(array(
	'name' => 'Golf Canada Members',
	'templates.path' => 'partials',
	'mode' => 'development',
	'debug' => true,
));


//----- App Routes

// Login
$app->post('/login', function () use ($app) {

	$username = $app->request()->params('username');
	$password = $app->request()->params('password');

	$memberGUID = getGN21User($username, $password);
	if($memberGUID['status'] == 'success') {
		
		$_SESSION['memberGUID'] = $memberGUID['response'];
		$app->redirect('./dashboard');
	
	} else {

		// display flash that use could not be found
		$app->flash('message', 'We could not find your member account. Please contact (email address for support)');
		$app->redirect('./');
	}
});

// Logout
$app->post('/logout', function () use ($app) {
	unset($_SESSION['memberGUID']);
});


/*----------------------------------------------------------*/


// Home
$app->get('/', function () use ($app) {
	if(isset($_SESSION['memberGUID'])) {
		$app->redirect('./dashboard');
	} else {
		$app->render('login.php');
	}
});

// Dashboard
$app->get('/dashboard', function () use ($app) {
	if(isset($_SESSION['memberGUID'])) {
		$app->render('dashboard.php');
	} else {
		$app->redirect('./');
	}
});

$app->get('/logout', function () use ($app) {
	if(isset($_COOKIE['loginUserCookie'])) {
		$app->render('logout.php');
	} else {
		$app->redirect('./');
	}
});

// 404
$app->notFound(function () use ($app) {
	$app->render('404.php');
});


//----- App Init
$app->run();