<?php
class WrapXML
{
	public $xml;
	
	function __construct( $xmlString )
	{
		if ($this->validate_xml($xmlString))
		{
			$this->xml = new DOMDocument();
			$this->xml->recover = true;
			$this->xml->formatOutput = true;
			$this->xml->preserveWhiteSpace = false;
			$this->xml->loadXML($xmlString) or die("Error"); // should already be validated
		}
		else
		{
			$this->xml = NULL;
		}
	}
	
	public function get_this()
	{
		return $this;
	}
	
	public function get_node_count( $nodeName )
	{
		$ret_val = 0;
		$oldNode = $this->xml->getElementsByTagName($nodeName);
		
		foreach ($oldNode as $on) 
		{
			$ret_val++;
		}
		return $ret_val;
	}
	
	// get_node_value returns the node value as a string
	public function get_node_value( $nodeName )
	{
		$ret_val = NULL;
		
		$oldNode = $this->xml->getElementsByTagName($nodeName);
		
		foreach ($oldNode as $on) 
		{
			$ret_val = $on->nodeValue;
			break;
		}
		return $ret_val;
	}

	public function get_attribute_value( $nodeName, $attribute_name )
	{
		$attVal;
		$searchNode = $this->xml->getElementsByTagName( $nodeName );
		foreach( $searchNode as $searchNode ) 
		{ 
    		$attVal = $searchNode->getAttribute($attribute_name); 
    		break;
    	}
    	return $attVal;
	}
	
	public function find_node_value_match( $nodeName, $nodeValue, $startIndex=0 )
	{
		// if a nodeValue is found, the index is returned. else 0 returned
		$tempIndex = 0;
		$index = $startIndex;
		$found = FALSE;
		$compareNode = $this->xml->getElementsByTagName($nodeName);
		//echo 'index: '.$index.'<br />';
		//echo $nodeName.'$compareNode->length: '.$compareNode->length.'<br />';
		
		if ($compareNode->length <= $startIndex+1)
		{
			$found = FALSE;
		}
		else
		{
			foreach ($compareNode as $cn) 
			{
				if ($tempIndex < $startIndex)
				{
					$tempIndex++;
				}
				else
				{
					if (strtoupper($cn->nodeValue) === strtoupper($nodeValue)) 
					{ 
						$found = TRUE;
						break;
					}
					else
					{
						$index++; 
					}
				}
			}
		}
		return $found?$index:-1;
	}
	
	public function count_match( $index1, $index2, $node1, $node2, $val1, $val2, $matchCounter )
	{
		//echo 'inside count_match: '.$matchCounter.'<br />';
		$matchCount = $matchCounter;
		
		if ($index1 != -1 && $index2 != -1)
		{
			$m1 = $this->find_node_value_match($node1, $val1, $index1);
			$m2 = $this->find_node_value_match($node2, $val2, $index2);
			if ($m1 != -1 && $m2 != -1)
			{
				if ($m1 == $m2)
				{
					$matchCount = $this->count_match(++$m1, ++$m2, $node1, $node2, $val1, $val2, ++$matchCount);
				}
				else if ($m1 < $m2)
				{
					$matchCount = $this->count_match(++$m1, $m2, $node1, $node2, $val1, $val2, $matchCount);
				}
				else
				{
					$matchCount = $this->count_match($m1, ++$m2, $node1, $node2, $val1, $val2, $matchCount);
				}
			}
		}
		return $matchCount;
	}
	
	public function change_node_value( $nodeName, $newValue )
	{
		$oldNode = $this->xml->getElementsByTagName($nodeName);
		
		foreach ($oldNode as $on) 
		{
			$on->nodeValue = $newValue;
			break;
		}
	}
	
	// updated May 13, 2013
	// fixed error with encoding é like chars
	public function get_node( $nodeName )
	{
		$ret_val = NULL;
		$oldNode = $this->xml->getElementsByTagName($nodeName);
		
		foreach ($oldNode as $on) 
		{
			$ret_val = html_entity_decode($on->ownerDocument->saveXML($on));
			break;
		}
		return $ret_val;
	}
	
	/*
	// get_node returns the full node as a string
	public function get_node( $nodeName )
	{
		$ret_val = NULL;
		$oldNode = $this->xml->getElementsByTagName($nodeName);
		
		foreach ($oldNode as $on) 
		{
			$tmpDOM  = new DOMDocument();
			$cloned = $on->cloneNode(TRUE);
			$tmpDOM->appendChild($tmpDOM->importNode($cloned, true) );
			$ret_val = $tmpDOM->saveHTML();
			break;
		}
		return $ret_val;
	}
	*/
	
	public function get_node_as_DOMElement( $nodeName )
	{
		$ret_val = $this->xml->getElementsByTagName($nodeName)->item(0);
//		$oldNode = $this->xml->getElementsByTagName($nodeName)->item(0);
		
//		foreach ($oldNode as $on) 
//		{
//			$ret_val = $on;
//			break;
//		}
		return $ret_val;
	}
	
	public function append_attribute( $nodeName, $attName, $attValue )
	{
		$domAttribute = $this->xml->createAttribute($attName);
		$domAttribute->value = $attValue;
		$this->xml->getElementsByTagName($nodeName)->item(0)->appendChild($domAttribute);
	}
	
	public function get_xml()
	{
		return $this->xml;
	}
	
	public function get_xml_as_string()
	{
		$returnXML = $this->xml->saveXML();
		return $returnXML;
	}
	
	public function get_xml_HTML_formatted()
	{
		$returnXML = $this->xml->saveXML();
		return "<xmp>". $returnXML ."</xmp>";
	}
	
	public function validate_xml( $xmlstr )
	{
		$valid = TRUE;
		// TODO: Write some validation code
		return $valid;
	}
	
	public function clear_ClubName()
	{
		$oldNode = $this->xml->getElementsByTagName('ClubName');
		
		foreach ($oldNode as $on) 
		{
			$on->nodeValue = '';
		}
	}
}
?>