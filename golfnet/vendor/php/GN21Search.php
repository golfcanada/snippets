<?php
//http://services.uat.ngn.com/courseservice.asmx
//http://services.uat.ngn.com/handicapservice.asmx
//http://services.uat.ngn.com/scoreservice.asmx
//http://services.uat.ngn.com/memberservice.asmx

class GN21Search
{
	private static $GN21UN     = 'RCGAMobileService';
	private static $GN21SOURCE = 'RCGAMobile';
	private static $GN21PW     = 'RvT^^e23has4'; // 'RvT^^e23has4' is the production password // golfnet is stage
	private static $uat        = ''; // just make this an empty string when hitting production // 'uat.' is stage
	
	public static function construct_soap_xml( $searchCriteria, $service, $function )
	{
		$soapXML = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	<soap:Body>
		<'. $function . 'Request xmlns="http://services.ngn.com/2007/03/20/' . $service . '">
			<TransactionId>string</TransactionId>
			<Credentials>
				<Username xmlns="http://services.ngn.com/2007/03/20/HeaderData">' . self::$GN21UN . '</Username>
				<Password xmlns="http://services.ngn.com/2007/03/20/HeaderData">' . self::$GN21PW . '</Password>
				<Source xmlns="http://services.ngn.com/2007/03/20/HeaderData">' . self::$GN21SOURCE . '</Source>
			</Credentials>'.
			$searchCriteria.
		'</'. $function . 'Request>
	</soap:Body>
</soap:Envelope>';

		return $soapXML;
	}
	
	public static function construct_UpdateMembersRequest( $updatedInfo )
	{
		$date = new DateTime();
		$transactionID = $date->getTimestamp();
		
		$soapXML = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	<soap:Body>
		<UpdateMembersRequest xmlns="http://services.ngn.com/2007/03/20/MemberService">
			<TransactionId>'.$transactionID.'</TransactionId>
			<Credentials>
				<Username xmlns="http://services.ngn.com/2007/03/20/HeaderData">'.self::$GN21UN.'</Username>
				<Password xmlns="http://services.ngn.com/2007/03/20/HeaderData">'.self::$GN21PW.'</Password>
				<Source xmlns="http://services.ngn.com/2007/03/20/HeaderData">'.self::$GN21SOURCE.'</Source>
			</Credentials>
			<MemberList>'.
				$updatedInfo.'
			</MemberList>
		</UpdateMembersRequest>
	</soap:Body>
</soap:Envelope>';	

		return $soapXML;
	}
	
	public static function gN21_API_call( $xml, $service, $function ) 
	{
		// http://services.uat.ngn.com/memberservice.asmx
		$searchURL = 'http://services.'.self::$uat.'ngn.com/memberservice.asmx';
		//echo '<p>Search URL: '.$searchURL.'</p>';
		
		$ch = curl_init($searchURL);
		
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('Host: services.'.self::$uat.'ngn.com','Content-Type: text/xml; charset=utf-8', 'Content-Length: '. strlen($xml), 
					'SOAPAction: "http://services.ngn.com/2007/03/20/' . $service . '/' . $function .'"'));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}
}

?>